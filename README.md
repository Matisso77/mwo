# YATI - Yet Another Transaction Initiator

## What is it?

Have you used [Tricount](https://www.tricount.com) or maybe [Splitwise](https://www.splitwise.com/)? If yes then you
know what our app does. If no then check them out!  

Add values:
* You can initiate money transfer directly from the app.
* You will be notified when your friend is starving to death and you owe him money (SHAME ON YOU!).
* You will be also notified when you finally are able to pay your debts (and again have nothing on your account).

Our solution will be presented in form of web application, but worry not we know how to make website mobile friendly.

## How to use it?
TBA

## Who are we?
We are a group of students from [Wrocław University of Science and Technology](http://ki.pwr.edu.pl/) who want to
graduate on time.

## What do we use?
This piece of software is written using a variety of technologies. The most notable are Python 3.7 with Flask 1.0 on
backend side and Vue.js on front.

## See any bugs? Tell us
There's bound to be a lot of errors, varying in magnitude. If you stumble upon this piece of software and decide
to take a look, please report any bugs you find to our scrum master @Matisso77 (a.k.a. jati).

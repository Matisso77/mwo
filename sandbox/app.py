import sys, time
from flask import Flask, jsonify, request

app = Flask(__name__)


### /v2_1_1.1/accounts/v2_1_1.1/getAccount - path accountInformations
@app.route('/v2_1_1.1/accounts/v2_1_1.1/getAccount', methods=['POST'])
def listsPost():
    """
    Tries to fetch a list of checklists.
    """
    rq = request.json
    print(rq)
    #Klasa odpowiedzi na zapytania o konto PSU
    res =  {
        #Klasa zawierająca zwrotne metadane
        "responseHeader": {
            #Identyfikator żądania otrzymanego od TPP
            "requestId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            #Data odpowiedzi, format: YYYY-MM-DDThh:mm:ss[.mmm]
            "sendDate": "2019-11-07T19:52:25.928Z",
            #Znacznik określający czy odpowiedz zostanie przekazana w formie wywołania zwrotnego. 
            #true - gdy odpowiedz w formie wywołania zwrotnego. Inna dopuszczalna wartość to false.
            "isCallback": True
        },
        #Klasa informacji o koncie
        "account": {
            #max 36
            "accountNumber": "string",
            #Klasa zawierająca dane nazwy i adresu w postaci czterech linii danych
            "nameAddress": {
            #1-4 długość
            "value": [
                "string"
            ]
            },
            #Klasa zawierająca dane elementu/pozycji słownika
            "accountType": {
                #Kod pozycji słownika 
                "code": "string",
                #Opis pozycji słownika
                "description": "string"
            },
            #Nazwa typu rachunku (definiowana przez ASPSP)
            "accountTypeName": "string",
            #Rodzaj posiadacza rachunku: osoba fizyczna lub osoba prawna Enum: [individual, corporation ]
            "accountHolderType": "individual",
            #Nazwa konta ustawiona przez klienta
            "accountNameClient": "string",
            #maxLength: 3 Waluta rachunku
            "currency": "string",
            #Dostępne środki - po wykonaniu transakcji
            "availableBalance": "string",
            #Saldo księgowe rachunku - po wykonaniu transakcji
            "bookingBalance": "string",
            #Klasa zawierająca dane banku
            "bank": {
                #Numer BIC/SWIFT Banku
                "bicOrSwift": "string",
                #Nazwa Banku
                "name": "string",
                #Klasa zawierająca dane nazwy i adresu w postaci czterech linii danych
                "address": [
                    # 1-4 długość
                    "string"
                ]
            },
            #Klasa mapy <string, string> 
            "auxData": {
                "additionalProp1": "string",
                "additionalProp2": "string",
                "additionalProp3": "string"
            }
        }
    }

    res2 =  {
    "responseHeader": {
        "requestId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "sendDate": "2019-11-07T19:56:00.213Z",
        "isCallback": True
    },
    "code": "string",
    "message": "ERROR"
    }
    try:
        return jsonify(res), 200
    except Exception as e:
        print(str(e))
        return jsonify(res2), 418



### /paymentsInitiation path - /v2_1_1.1/payments/v2_1_1.1/domestic
@app.route('/v2_1_1.1/payments/v2_1_1.1/domestic', methods=['POST'])
def listsPost2():
    """
    Tries to fetch a list of checklists.
    """
    rq = request.json
    print(rq)
    res =  {
        #Klasa odpowiedzi zlecenia płatności
        "responseHeader": {
            #Identyfikator żądania otrzymanego od TPP 
            "requestId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            #Data odpowiedzi, format: YYYY-MM-DDThh:mm:ss[.mmm]
            "sendDate": "2019-11-07T19:56:00.231Z",
            #Znacznik określający czy odpowiedz zostanie przekazana w formie wywołania zwrotnego. 
            #true - gdy odpowiedz w formie wywołania zwrotnego. Inna dopuszczalna wartość to false.
            "isCallback": True
        },
        #Identyfiaktor płatności
        "paymentId": "string",
        #Słownik statusów płatności Enum: [submitted, cancelled, pending, done, rejected, scheduled ]
        "generalStatus": "submitted",
        #Status płatności 
        "detailedStatus": "string"
    }

    res2 =  {
    "responseHeader": {
        "requestId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "sendDate": "2019-11-07T19:56:00.233Z",
        "isCallback": True
    },
    "code": "string",
    "message": "ERROR"
    }
    try:
        return jsonify(res), 200
    except Exception as e:
        print(str(e))
        return jsonify(res2), 418


app.run(debug=True, host='127.0.0.1', port=443)
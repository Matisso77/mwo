from config import db
from models import *
import sqlalchemy
from flask_login import login_manager

def getUserByID(uid):
    """
    Auxillary login functionality - return username by ID
    """
    return UserLogin.query.get(int(uid))

def getUIDByName(username):
    try:
        usr = db.session.query(UserLogin).filter(UserLogin.username == username).one_or_none()
        return usr.uid
    except AttributeError:
        print(f'[ERROR] User not found: {username}')
    except sqlalchemy.orm.exc.MultipleResultsFound:        
        print(f'[ERROR] Multiple records found for user: {username}')
    finally:
        return None

def getGIDByName(groupName):
    try:
        grp = db.session.query(GroupInfo).filter(GroupInfo.name == groupName).one_or_none()
        return grp.gid
    except AttributeError:
        print(f'[ERROR] Group not found: {groupName}')
    except sqlalchemy.orm.exc.MultipleResultsFound:        
        print(f'[ERROR] Multiple records found for group: {groupName}')
    finally:
        return None
        

def getLogin(username):
    """
    Main login functionality - checks if a username pair exists in the db
    """
    return UserLogin.query.filter(UserLogin.username == username).one_or_none()

def register(username, password, email):
    try:
        new_user = UserLogin(uid=None, username=username, password=password, email=email)
        db.session.add(new_user)
        db.session.add(UserConfig(uid=new_user.uid, default_currency='PLN', timezone='UTC'))
        db.session.add(UserAccount(uid=new_user.uid, account_number='', api_key=''))
        db.session.commit()
        return True
    except:
        db.session.rollback()
        return False

def usernameExists(username):
    return True if UserLogin.query.filter(UserLogin.username == username).one_or_none() else False

def emailExists(email):
    return True if UserLogin.query.filter(UserLogin.email == email).one_or_none() else False

def getUserSettings(username):
    uid = getUIDByName(username)
    config = UserConfig.query.filter(UserConfig.uid == uid).one_or_none()
    if config:
        return {
            'default_currency' : config.default_currency, 
            'timezone' : config.timezone
        }
    else:
        print(f'[ERROR] Config not found for: {username}')
        return {
            'default_currency' : 'PLN', 
            'timezone' : 'UTC'
        }

def getUserGroups(username):
    uid = getUIDByName(username)
    groups = []
    if uid:
        data = db.session.query(GroupMember, GroupInfo).filter(GroupMember.uid == uid).all()
        for (gm, gi) in data:
            groups += {
                'name' : gi.name,
                'icon' : gi.image,
                'default_currency' : gi.default_currency,
                'debt_simplify' : gi.debt_simplify
            }
    return groups

def getGroupData(groupName):
    #Group params, transaction history, balances relative to logged user
    #return res = {'username': user.username,'total_balance': 20.05, 'friend_balances': [{'friend': 'Mati', 'amount': 20.05, 'currency': 'PLN'}], 'user_config': {'default_currency': 'PLN', 'timezone': 'UTF-8'}}
    pass

def fetchUserState(data):
    #return DICT {total_balance: $bal, friend_balances: [{friend: ID, amount: $$, currency: CBN}, ...], groups: [{}], user_config: {opt1: val, opt2:val}}
    username = data.get('username')

    groups = getUserGroups(username)
    config = getUserSettings(username)
    #return {
    # 'total_balance': ,
    # 'friend_balances': , 
    # 'groups': groups,
    # 'user_config': config
    # }

def groupExists(groupName):
    return True if GroupInfo.query.filter(GroupInfo.name == groupName).one_or_none() else False

def createGroup(groupName, icon, default_currency):
    try:
        new_group = GroupInfo(gid=None, name=groupName, image=icon, default_currency=default_currency)
        db.session.add(new_group)
        db.session.commit()
        return True
    except:
        db.session.rollback()
        return False

def joinGroup(groupName, uid):
    gid = getGIDByName(groupName)
    try:
        new_membership = GroupMember(gid=gid, uid=uid)
        db.session.add(new_membership)
        db.session.commit()
        return True
    except:
        db.session.rollback()
        return False

def addFriend(uid, username):
    uid2 = getUIDByName(username)
    try:
        new_friendship = Friendship(rid=None, uid_gets=uid, uid_pays=uid2)
        db.session.add(new_friendship)
        db.session.commit()
        return True
    except:
        db.session.rollback()
        return False

def addNewTransaction(user1, user2, amount, currency='PLN'):
    pass

def getTransactionHistory(username):
    pass

def updateBalances():
    pass

        # https://www.tutorialspoint.com/sqlalchemy/sqlalchemy_orm_working_with_joins.htm
# # TEMP
# q = (Session.query(User,Document,DocumentPermissions)
#     .filter(User.email == Document.author)
#     .filter(Document.name == DocumentPermissions.document)
#     .filter(User.email == 'someemail')
#     .all())